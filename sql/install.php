<?php
/**
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2019 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/
$sql = array();

$sql[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'mcustomercallback` (
    `id_mcustomercallback` int(11) NOT NULL AUTO_INCREMENT,
    `phone_number` varchar(15) NOT NULL,
    `id_product` int(11) NOT NULL,
    PRIMARY KEY  (`id_mcustomercallback`)
) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;';

$sql[] = 'ALTER TABLE ' . _DB_PREFIX_ . 'mcustomercallback MODIFY phone_number varchar(15) NOT NULL;';

$check_id_product_column = Db::getInstance()->getRow("SELECT EXISTS(
	SELECT * FROM information_schema.columns
	WHERE table_schema = database()
	AND COLUMN_NAME = 'id_product'
	AND table_name = '" . _DB_PREFIX_ . "mcustomercallback') AS t"
);
    
if (!$check_id_product_column['t']) {
    $sql[] = 'ALTER TABLE ' . _DB_PREFIX_ . 'mcustomercallback ADD COLUMN id_product int(11) NOT NULL;';
}

foreach ($sql as $query) {
    if (Db::getInstance()->execute($query) == false) {
        return false;
    }
}
