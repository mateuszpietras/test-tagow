<?php
/**
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2019 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

require_once('classes/Callback.php');

class Mcustomercallback extends Module
{
    protected static $contact_fields = array(
        'bo_email',
        'min_phone_length'
    );

    protected $config_form = false;
    public $errors = [];
    public $id_shop;
    public $id_lang;

    public function __construct()
    {
        $this->name = 'mcustomercallback';
        $this->tab = 'front_office_features';
        $this->version = '1.1.0';
        $this->author = 'Mateusz Pietras';
        $this->need_instance = 1;

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Customer call back');
        $this->description = $this->l('module gives customer option to leave a phone number to call back');

        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        Configuration::updateValue('MCUSTOMERCALLBACK_LIVE_MODE', false);

        include(dirname(__FILE__).'/sql/install.php');

        return parent::install() &&
            $this->registerHook('header') &&
            $this->registerHook('displayCallBack');
    }

    public function uninstall()
    {
        Configuration::deleteByName('MCUSTOMERCALLBACK_LIVE_MODE');

        include(dirname(__FILE__).'/sql/uninstall.php');

        foreach (Mcustomercallback::$contact_fields as $field)
            Configuration::deleteByName($field);
        return (parent::uninstall());
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {

        if (((bool)Tools::isSubmit('submitMcustomercallbackModule')) == true) {
            $this->postProcess();
        }

        $this->context->smarty->assign('module_dir', $this->_path);

        $output = $this->context->smarty->fetch($this->local_path.'views/templates/admin/configure.tpl');

        return $output.$this->renderForm();
    }

    /**
     * Create the form that will be displayed in the configuration of your module.
     */
    protected function renderForm()
    {

        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Settings'),
                    'icon' => 'icon-cogs'
                ),
                'input' => array(
                    array(
                        'type' => 'text',
                        'label' => $this->l('E-mail'),
                        'name' => 'bo_email',
                        'hint' => $this->l('e-mail to send an alert (if empty, alert will be send to default shop e-mail)'),
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Number length'),
                        'name' => 'min_phone_length',
                        'hint' => $this->l('Minimal phone number length (if empty, length will be set to 5)'),
                    )
                ),
                'submit' => array(
                    'title' => $this->l('Save')
                )
            ),
        );

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);
        $this->fields_form = array();

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitMcustomercallbackModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => array(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );
        foreach (Mcustomercallback::$contact_fields as $field)
            $helper->tpl_vars['fields_value'][$field] = Tools::getValue($field, Configuration::get($field));
         return $helper->generateForm(array($fields_form));
    }

    /**
     * Create the structure of your form.
     */
    protected function getConfigForm()
    {
        return array(
            'form' => array(
                'legend' => array(
                'title' => $this->l('Settings'),
                'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Live mode'),
                        'name' => 'MCUSTOMERCALLBACK_LIVE_MODE',
                        'is_bool' => true,
                        'desc' => $this->l('Use this module in live mode'),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => true,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => false,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'prefix' => '<i class="icon icon-envelope"></i>',
                        'desc' => $this->l('Enter a valid email address'),
                        'name' => 'MCUSTOMERCALLBACK_ACCOUNT_EMAIL',
                        'label' => $this->l('Email'),
                    ),
                    array(
                        'type' => 'password',
                        'name' => 'MCUSTOMERCALLBACK_ACCOUNT_PASSWORD',
                        'label' => $this->l('Password'),
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );
    }

    /**
     * Save form data.
     */
    protected function postProcess()
    {

        foreach (Mcustomercallback::$contact_fields as $field)
            Configuration::updateValue($field, Tools::getValue($field));
        $html = $this->displayConfirmation($this->l('Configuration updated'));
    }


    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     */
    public function hookHeader()
    {
        $this->context->controller->addJS($this->_path.'/views/js/mcustomercallback.js');
        $this->context->controller->addCSS($this->_path.'/views/css/mcustomercallback.css');

        $num_length = '';

        foreach (Mcustomercallback::$contact_fields as $field)
            $callbackDatas[$field] = Tools::getValue(($field), Configuration::get($field));

        if ($callbackDatas['min_phone_length'] == ''){
            $num_length = 5;
        }
        else {
            $num_length = $callbackDatas['min_phone_length'];
        }
        
        $this->context->smarty->assign([
            'min_phone_length' => (int)$num_length
        ]);

    }

    public function hookDisplayCallBack($params)
    {
        if (((bool)Tools::isSubmit('savecustomercallback')) == true) {
            $this->submitForm($params);
        }
        $this->context->smarty->assign([
            'product' => $params['product']
        ]);

        return $this->display(__FILE__, 'mcustomercallback.tpl');
    }

    public function validateInput() {

        $input = Tools::getValue('callback_phone');
        $num_length = '';

        foreach (Mcustomercallback::$contact_fields as $field)
            $callbackDatas[$field] = Tools::getValue(($field), Configuration::get($field));

        if ($callbackDatas['min_phone_length'] == ''){
            $num_length = 5;
        }
        else {
            $num_length = $callbackDatas['min_phone_length'];
        }

        if(empty($input)) {
            $this->errors[] = $this->l('Phone number is required');
        }
        if(!Validate::isPhoneNumber($input)) {

            $this->errors[] = $this->l('Phone number is not valid');
        }
        if(strlen($input) < $num_length) {

            $this->errors[] = $this->l('Phone number is too short');
        }

        return count($this->errors) > 0 ? $this->errors : false;
    }

    public function submitForm($params) {

        $id_product = $params['product']->id;

        if($errors = $this->validateInput()) {
            $this->context->smarty->assign(['errors' => $errors]);
            return;
        }
        
        $callback = new Callback;
        $callback->phone_number = Tools::getValue('callback_phone');
        $callback->id_product = (int)$id_product;
        
    }

    public function ajaxSubmitCustomerCallback() {
        
        $id_shop = Context::getContext()->shop->id;

        $id_lang = Context::getContext()->language->id;

        $iso = Language::getIsoById($id_lang);

        $id_product = (int)Tools::getValue('id_product');

        $result = [
            'status' => false,
            'hasError' => false,
        ];

        if($errors = $this->validateInput()) {
            $result['errors'] = $errors;
            $result['hasError'] = true;
        }
        
        $callback = new Callback;
        $callback->phone_number = Tools::getValue('callback_phone');
        $callback->id_product = (int)$id_product;

        $product = new Product;
        $product_name = $product->getProductName($id_product, null, $id_lang);

        $link = new Link;
        $product_link = $link->getProductLink($id_product, null, null, null, $id_lang);
        
        $dest_email = '';

        foreach (Mcustomercallback::$contact_fields as $field)
            $callbackDatas[$field] = Tools::getValue(($field), Configuration::get($field));
        
        if ($callbackDatas['bo_email'] == ''){
            $dest_email = Configuration::get('PS_SHOP_EMAIL', null, null, $id_shop);
        }
        else {
            $dest_email = $callbackDatas['bo_email'];
        }

        if($result['hasError']) {
            $result['status'] = false;
        } else {
            $result['status'] = $callback->add();

            $template_vars = array(
                '{phone_number}' => $callback->phone_number,
                '{product_name}' => $product_name,
                '{product_link}' => $product_link
            );

            if (file_exists(dirname(__FILE__).'/mails/'.$iso.'/customer_callback.txt') &&
                file_exists(dirname(__FILE__).'/mails/'.$iso.'/customer_callback.html'))
                Mail::Send(
                    $id_lang,
                    'customer_callback',
                    Mail::l('Client asks for contact', $id_lang),
                    $template_vars,
                    $dest_email,
                    null,
                    (string)Configuration::get('PS_SHOP_EMAIL', null, null, $id_shop),
                    (string)Configuration::get('PS_SHOP_NAME', null, null, $id_shop),
                    null,
                    null,
                    dirname(__FILE__).'/mails/',
                    false,
                    $id_shop
                );

            $result['confirm'] = $this->l('Your number has been saved. We will contact you as soon as possible');
        }
        
        return Tools::jsonEncode($result);

    }

}
