<?php

class Callback extends ObjectModel
{
    public $id_callback;
    public $phone_number;
    public $id_product;

    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'mcustomercallback',
        'primary' => 'id_callback',
        'fields' => array(
            'phone_number' => array('type' => self::TYPE_INT, 'validate' => 'isPhoneNumber', 'required' => true),
            'id_product' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => true),
        )
    );

    public function __construct($id = null, $id_lang = null, $id_shop = null)
    {
        parent::__construct($id, $id_lang, $id_shop);
    }
}