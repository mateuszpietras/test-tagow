$(document).on('submit', '#callback_form', function(e) {
    e.preventDefault();
    var _this = $(this);
    var formData = $('#callback_form').serialize();
    var errors = '';

    $('#savecustomercallback').attr('disabled', 'disabled');

    var phone = $('#callback_phone')
    var minlength = phone.attr('minlength');
    var phoneVal = phone.val();
    var pattern = /^[+0-9. ()-]+$/;
    var isValidPhone = phoneVal.length == 0 ? true : (pattern.test(phoneVal) && phoneVal.length >= minlength);

    if(minlength == callbackPhoneLength){
        
        if(isValidPhone){
    
        var query = $.ajax({
            type: 'POST',
            url: baseDir + 'modules/mcustomercallback/mcustomercallback-ajax.php',
            data: 'method=addCallback&id_product=' + id_product + '&' + formData,
            dataType: 'json',
            success: function(result) {
                if(result.hasError) {
                    $.each(result.errors, function(key, error) {
                        errors += '<p>' + error + '</p>';
                    });
                    $('#callbackAlertSuccess').hide();
                    $('#callbackAlertError').html(errors).fadeIn();
                    $('#savecustomercallback').removeAttr('disabled');
                    phone.parent().addClass('form-error');
                } else {
                    $('#callbackAlertError').hide();
                    phone.parent().addClass('form-ok');
                    setTimeout(function(){
                        $('.mcustomercallback_header').hide();
                        _this.remove();
                        $('#callbackAlertSuccess').html(result.confirm).fadeIn();
                    }, 1000);
                }
            }
        });

        } else {
            errors += '<p>' + phoneNotValidText + '</p>';
            $('#callbackAlertError').html(errors).fadeIn();
            $('#savecustomercallback').removeAttr('disabled');
            phone.parent().addClass('form-error');
        }
        
    } else {
        errors += '<p>' + phoneLengthNotValidText + '</p>';
        $('#callbackAlertError').html(errors).fadeIn();
        $('#savecustomercallback').removeAttr('disabled');
        phone.parent().addClass('form-error');
    }
});

$(document).ready(function(){
  $('[data-toggle="callback_tooltip"]').tooltip(); 
});