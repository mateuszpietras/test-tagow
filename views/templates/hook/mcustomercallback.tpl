<a data-fancybox="" data-src="#mcustomercallback" data-toggle="callback_tooltip" data-placement="top" title="Enter your phone number to find out more about this product" href="#" class="btn btn-info mt-10 mb-10">{l s="Leave your phone number" mod="mcustomercallback"}</a>
<div id="mcustomercallback" class="mcustomercallback text-center fancybox" style="display: none;">
	<div class="mcustomercallback_header">
		<h4 class="modal-title" id="modalLabel">{l s='Leave us your phone number' mod='mcustomercallback'}</h4>
	</div>
	<div class="alert alert-success m-0" id="callbackAlertSuccess" style="display: none;"></div>
	<div class="alert alert-danger" id="callbackAlertError" style="display: none;"></div>
	<form method="post" id="callback_form">
		<div class="mcustomercallback_body">
			<div class="h5 mb-25">
				<p>{l s='Here you can leave your phone number. We will call as soon as possible.' mod='mcustomercallback'}</p>
			</div>
			<div class="form-group">
				<label for="callback_phone">{l s='Phone number' mod='mcustomercallback'}</label>
				<input class="form-control" id="callback_phone" name="callback_phone" minlength="{$min_phone_length}" required>
			</div>
		</div>
		<div class="mcustomercallback_footer text-right">
			<button type="submit" class="btn btn-primary" name="savecustomercallback" id="savecustomercallback">{l s='Save' mod='mcustomercallback'}</button>
		</div>
	</form>
</div>

{addJsDefL name=callbackPhoneLength}{$min_phone_length}{/addJsDefL}
{addJsDefL name=phoneNotValidText}{l s='Phone number is not valid' js=1 mod="mcustomercallback"}{/addJsDefL}
{addJsDefL name=phoneLengthNotValidText}{l s='Phone length parameter is not valid' js=1 mod="mcustomercallback"}{/addJsDefL}